
$(document).ready(function(){
    const BOTURL = "localhost/canarychat/admin/";
    const SOCKETURL = "localhost:8443";
    var spamCounter = 0; var resumeCounter = 0;
    var adminStatus = false;
    // var emailPrompted = false;
    // var validatingEmail = false;
    var userId;
    var socket;
    
    // notifications
    if (!window.Notification) {
        console.log('Browser does not support notifications.');
    } else {
        if (Notification.permission !== 'granted') {
            Notification.requestPermission().then(function(p) {
                if(p === 'granted') {
                    // show notification here
                } else {
                    console.log('User blocked notifications.');
                }
            }).catch(function(err) {
                console.error(err);
            });
        }
    }

    function popNotification(title, body) {
        if (Notification.permission === 'granted') {
            new Notification(title, {
                body: body,
                icon: "/img/favicon.svg"
            });
        } else {
            Notification.requestPermission().then(function(p) {
                console.log("Show notifications enabled!");
            }).catch(function(err) {
                console.error(err);
            });
        }
    }

    // websocket
    function initSocket() {
        if (localStorage.getItem("canarychatId")) {
            userId = localStorage.getItem("canarychatId");
        } else {
            userId = "u-"+Math.round(new Date().getTime()/1000);
            localStorage.setItem("canarychatId", userId);
        }
        console.log(userId);
        socket = new WebSocket(SOCKETURL+"?user=" + userId);
    }
    initSocket();
    
    socket.onmessage = function(event) {
        var data = JSON.parse(event.data);
        if (data.type == "seed-message") {
            var messages = data.body;
            messages.forEach((message)=>{
                var type = message.admin? "bot": "user";
                appendChatBalloon(message.body, type);
            })
        } else if (data.type == "ui-admin-status") {
            // update admin status indicator
            var status = data.body == true? "online": "offline"; 
            adminStatus = data.body;
            $(".online-indicator").removeClass("online");
            $(".online-indicator").removeClass("offline");
            $(".online-indicator").addClass(status);
            if (data.body) {
                $(".online-indicator").attr("title", "Admin is online");
                $(".chat-status").html("");
            } else {
                $(".online-indicator").attr("title", "Admin is away");
                // $(".canarybot-status").html("CanaryBot is hybernating right now but leave your messages and we'll look into it when we wake up!<br>A bot will try to answer your questions for the mean time.");
            }
        } else if (data.type == "message") {
            if (data.admin == true) {
                appendChatBalloon(data.body, "bot");
            } else {
                appendChatBalloon(data.body, "user");
            }
        } else {
            console.log(data);
        }
    }
    socket.onopen = function() {
        console.log("Chat connection open!");
    }
    socket.onclose = function(event) {
        console.warn(event);
        $(".online-indicator").removeClass("online");
        $(".online-indicator").addClass("offline");
        // $(".canarybot-status").html("CanaryBot is hybernating right now but leave your messages and we'll look into it when we wake up!<br>A bot will try to answer your questions for the mean time.");
    }
    socket.onerror = function(event) {
        console.error("Cannot connect to socket server.");
    }
    
    
    // CHATBOX
    function appendChatBalloon(text,type) {
        const balloon = '<div class="chat-balloon '+type+'"><div class="message">'+text+'</div></div>';
        $(".chat-messages").append(balloon).animate({
            scrollTop: 9999
        }, 100)
    }
    
    function send(text) {
        if (socket.readyState === WebSocket.OPEN) {
            var msg = {
                "admin": false,
                "type": "message",
                "body": text
            };
            socket.send(JSON.stringify(msg));
            // check for auto response if admin is offline
            if (!adminStatus) {
                askBot(text,false);
            }
        } else {
            // check for auto response if socket server is offline
            askBot(text,true);
        }
    
    }
    
    function askBot(text, logFlag) {
        $.ajax({
            url: BOTURL+"bot.php",
            crossDomain: true,
            type: "POST",
            data: {q:text, userId: userId, logChat: logFlag},
            success: function(data, status, xhr) {
                console.log(data);
                if (data.length > 2) {
                    var res = JSON.parse(data);
                    if (res.body) {
                        appendChatBalloon(res.body,"bot");
                    }
                }
            },
            error: function(data, status, er) {
                console.error(er);
            }
        })
    }
        
    $(document).on("keyup", ".textarea>textarea", function(e){
        if(e.which == 13) {
            spamCounter++;
            if (spamCounter > 5) {
                spammed();
            } else {
                var text = $(this).val();
                text = text.trim();
                text = text.replace(/(<([^>]+)>)/ig,"");
    
                if (text.length) {
                    // if (text.length <= 250) {
                        appendChatBalloon(text, "user");
                        send(text);
                    // } else {
                    //     setTimeout(() => {
                    //         appendChatBalloon("Sorry, your question is too long. <br>Please shorten it in a few words for me to understand.", "bot");
                    //     }, 1000);
                    // }
    
                }
                $(this).val("");
            }
        }
    });
    
    var spamInterval; var spamRunning = false;
    function spammed () {
        if (!spamRunning) {
            $(".chat-box>textarea").val("").attr("disabled", true).attr("placeholder","Hold your horses...10");
            spamRunning = true;
            resumeCounter = 9;
            spamInterval = setInterval(function(){
                $(".chat-box>textarea").val("").attr("disabled", true).attr("placeholder","Hold your horses..."+resumeCounter);
                resumeCounter --;
                if (resumeCounter == -1) {
                    clearInterval(spamInterval);
                    $(".chat-box>textarea").attr("disabled", false).attr("placeholder","Type here...").focus();
                    spamCounter = 0;
                    spamRunning = false;
                }
            }, 1000);
        }
    }
    
    setInterval(function(){
        spamCounter = 0;
    }, 10000);
    
});
    