
$(document).ready(function(){
    const BOTURL = "localhost/canarychat/admin/";
    const SOCKETURL = "localhost:8443/?a=true";
    const urlParams = new URLSearchParams(window.location.search);
    const to = urlParams.get('to');
    var socket;
    var CONNECTIONS = []; var MESSAGES = [];
    var reloading = false;

    // notifications
    if (!window.Notification) {
        console.log('Browser does not support notifications.');
    } else {
        if (Notification.permission !== 'granted') {
            Notification.requestPermission().then(function(p) {
                if(p === 'granted') {
                    // show notification here
                } else {
                    console.log('User blocked notifications.');
                }
            }).catch(function(err) {
                console.error(err);
            });
        }
    }
    function popNotification(title, body) {
        if (Notification.permission === 'granted') {
            new Notification(title, {
                body: body,
                icon: "/img/favicon.svg"
            });
        } else {
            Notification.requestPermission().then(function(p) {
                console.log("Show notifications enabled!");
            }).catch(function(err) {
                console.error(err);
            });
        }
    }
    
    // load page content
    function clear() {
        $(".connections-section>.list>.li").each(function(){ $(this).remove(); });
        $(".chat-balloon").each(function(){ $(this).remove(); })
    }

    function getConnections(connections) {
        CONNECTIONS = [];
        $.getJSON("/canarychat/admin/connections.json", function(data){
            connections(data.filter(function(d) {
                return !d.admin;
            }));
        });
    }

    function displayConnections() {
        CONNECTIONS.forEach(function(conn,idx) {
            var status = "";
            if (conn.userId == to) {
                status = "active";
                MESSAGES = conn.messages;
            }
            if (!conn.alive) {
                status = "inactive";
            }
            $(".connections-section>.list").append('<div class="li row" id="'+conn.userId+'"><a href="?to='+(conn.userId)+'" class="'+status+' col">'+(conn.userId)+'</a><button class="del-connection '+(conn.alive?'hidden':'')+'" userId="'+(conn.userId)+'"><i class="fas fa-times"></i></button></div>');
        
            // display messages
            MESSAGES.forEach(function(message, idx) {
                var type = message.admin? "bot": "user";
                appendChatBalloon(message.body, type);
            });
        })
    }

    function reload() {
        reloading = true;
        clear();
        getConnections(function(connections) {
            CONNECTIONS = connections;
            // redirects clear
            if (connections.length == 0 && (to != undefined || to != null)) {
                window.location.href = "/canarychat/admin";
            } else if ((to == undefined || to == null) && CONNECTIONS.length > 0) {
                window.location.href = "/canarychat/admin/?to=" + CONNECTIONS[0].userId;
            }
            
            displayConnections();
            $(".chat-status").html("<strong>"+(to==null? "":to)+"&nbsp;<i class='fas fa-user-circle'></i></strong>");
            reloading = false;
        });
    }
    if(reloading == false) {
        reload();
    }

    // WEBSOCKET
    try {
        socket = new WebSocket(SOCKETURL);
    } catch (e) {
        console.warn("Cannot connect to socket server", e);
    }

    socket.onmessage = function(event) {
        var data = JSON.parse(event.data);
        if (data.type == "notice") {
            appendChatBalloon(data.body,"error");
        } else if (data.type == "ui-user-status") {
            if (data.body.alive) {
                $("#"+data.body.userId+" a").removeClass("inactive");
                $("#"+data.body.userId+" .del-connection").addClass("hidden");
                if (to == data.body.userId) {
                $("#"+data.body.userId+" a").addClass("active");
                }
            } else {
                $("#"+data.body.userId+" a").addClass("inactive");
                $("#"+data.body.userId+" .del-connection").removeClass("hidden");
            }
        } else if (data.type == "message") {
            if (data.to == to) {
                appendChatBalloon(data.body,"user");
            }
            // popNotification(data.to+" said:", data.body);
        } else if (data.type == "ui-new-connection") {
            console.log("New user connection",data.body.userId);
            popNotification("User Connected!", data.body.userId);
            $(".connections-section>.list").append('<div class="li row" id="'+data.body.userId+'"><a href="?to='+(data.body.userId)+'" class="col">'+(data.body.userId)+'</a><button class="del-connection hidden" userId="'+(data.body.userId)+'"><i class="fas fa-times"></i></button></div>');

            if (CONNECTIONS.length == 0) {
                reload();
            }
        } else if (data.type == "ui-admin-status") {
            // console.log("STATUS", data.body);
        } else if (data.type == "seed-message") {
            console.log("SEED", data.body);
        } else {
            console.warn("UNKNOWN MESSAGE TYPE",data);
        }
    }
    socket.onopen = function() {
        console.log("Chat connection open!");
    }
    socket.onclose = function(event) {
        console.warn(event);
        appendChatBalloon("Can't connect to server.","error");
    }
    socket.onerror = function(event) {
        // console.error("SOCKET ERROR",event);
    }

    function appendChatBalloon(text, type) {
        const balloon = '<div class="chat-balloon '+type+'"><div class="message">'+text+'</div></div>';
        $(".chat-messages").append(balloon).animate({ scrollTop: 9999 }, 100)
    }

    function send(text) {
        text = text.trim();
        appendChatBalloon(text,"bot");
        const msg = {
            "admin": true,
            "type": "message",
            "body": text,
            "to": to
        };
        if (socket.readyState === WebSocket.OPEN) {
            socket.send(JSON.stringify(msg));
        } else {
            $.ajax({
                url: BOTURL+"post.php",
                type: "POST",
                data: {action: "sendOffline", message: msg},
                success: function(data) {
                    console.log(data);
                }
            })
        }
    }

    $(document).on("keyup", ".textarea>textarea", function(e){
        if (e.key === "Enter") {
            e.preventDefault();
            const text = $(this).val();
            send(text);
            $(this).val("");
        }
    });

    $(document).on("click", ".del-connection", function(){
        var id = $(this).attr("userId");
        $(this).closest("li").remove();
        $.ajax({
            url: BOTURL+"post.php",
            type: "POST",
            data: {"action": "deleteConnectionByUserId", "userId":id},
            success: function(data) {
                console.log(data);
                var res = JSON.parse(data);
                if (to == res.body) {
                    $(".chat-balloon").each(function(){ $(this).remove(); })
                }
                if (reloading == false) {
                    reload();
                }
            }
        })
    });

    $(document).on("click", ".clear-inactive", function(){
        $.ajax({
            url: BOTURL+"post.php",
            type: "POST",
            data: {"action": "clearInactive"},
            success: function(data) {
                var res = JSON.parse(data);
                if (res.success) {
                    if (reloading == false) {
                        reload();
                    }
                }
            }
        })
    });

    $(document).on("click", ".refresh", function(){ 
        if (reloading == false) {
            reload();
        }
     });

    var connSectionToggle = false;
    $(document).on("click", ".connections-section .toggle-btn", function() {
        if (!connSectionToggle) {
            $(".connections-section").animate({
                left: "0px"
            }, 300);
            connSectionToggle = true;
        } else {
            $(".connections-section").animate({
                left: "-60vw"
            }, 300);
            connSectionToggle = false;
        }
    });

});