<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <title>CanaryChat</title>
    <meta name="description" content="CanaryChat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/canarychat/assets/style.css?v=<?php echo time() ?>">
</head>
<body>
    <div class="container">
        <div class="row header align-items-center">
            <img src="/img/favicon.svg" alt="logo" draggable="false" class="logo">
            <div>
                <h3>CanaryChat&nbsp;<i class="far fa-comment"></i></h3>
            </div>
            <div class="text-right col"><i class="fas fa-dot-circle online-indicator" title="Admin bot is away"></i></div>
        </div>

        <div class="chat-box row">
            <div class="chat-messages col">
                <!-- chat balloons here -->
            </div>
        </div>

        <div class="textarea row"><textarea name="chat-input" id="chat-input" placeholder="type here..." autofocus></textarea></div>

    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://kit.fontawesome.com/e94e85d680.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/canarychat/assets/script.js?v=<?php echo time() ?>"></script>
</body>
</html>
