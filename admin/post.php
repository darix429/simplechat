<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class AdminActions {
    public $file;
    private static $instance = null;

    private function __construct() {
        $this->file = "connections.json";
    } 

    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new AdminActions();
        }
        return self::$instance;
    }

    public function getConnections() {
        return file_exists($this->file)? json_decode(file_get_contents($this->file)) : [];
    }
    
    public function deleteConnectionByUserId($data) {
        try {
            
            $id = $data["userId"];
            $connections = $this->getConnections();
            $i=0;
            foreach($connections as $conn) {
                if ($conn->userId == $id) {
                    unset($connections[$i]);
                }
                $i++;
            }
            $connections = array_values($connections);
            file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));

            echo json_encode(array(
                "success"=>true,
                "body"=>$id
            ));
        }catch(Exception $e) {
            echo json_encode(array(
                "success"=>false,
                "body"=>$e->getMessage()
            ));
        }
    }

    public function clearInactive($data) {
        try {
            $connections = $this->getConnections();
            $i=0;
            foreach($connections as $conn) {
                if (!$conn->alive) {
                    unset($connections[$i]);
                }
                $i++;
            }
            $connections = array_values($connections);
            file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));

            echo json_encode(array(
                "success"=>true
            ));
        }catch(Exception $e) {
            echo json_encode(array(
                "success"=>false
            ));
        }
    }

    public function sendOffline($data) {
        $connections = $this->getConnections();
        $message = json_encode($data["message"]);
        $message = json_decode($message);
        $userId = $message->to;
        if ($userId) {
            $i=0;
            foreach($connections as $conn) {
                if($conn->userId == $userId) {
                    if (isset($conn->messages)) {
                        array_push($conn->messages, $message);
                    } else {
                        $conn->messages = [$message];
                    }
                    $connections[$i] = $conn;
                }
                $i++;
            }
            file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));
            echo json_encode(array(
                "success"=>true
            ));
        } else {
            echo json_encode(array(
                "success"=>false,
                "body"=>"User Id not set"
            ));
        }

    }
}


$action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING);
if (!empty($action))
AdminActions::getInstance()->$action($_POST);