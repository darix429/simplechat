<?php
    $to = filter_input(INPUT_GET, "to", FILTER_SANITIZE_STRING);
?>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
<title>CanaryChat Admin</title>
<meta name="description" content="CanaryChat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="/canarychat/assets/admin.css?v=<?php echo time() ?>">
</head>
<body>
    <div class="housing">

        <div class="d-inline-flex align-items-center header">
            <img src="/canarychat/assets/favicon.svg" draggable="false" class="logo">
            <div>
                <h3>CanaryChat  <i class="far fa-comment"></i></h3>
            </div>
        </div>

        <div class="d-inline-flex align-items-stretch chat-box">
            <div class="connections-section">
                <div class="connection-actions d-flex">
                    <button class="flex-grow-1 refresh">Refresh List</button><button class="flex-grow-1 clear-inactive">Clear Inactive</button>
                </div>
                <div class="list"></div>
                <button class="toggle-btn">
                    U<br>S<br>E<br>R<br>S
                </button>
            </div>
            <div class="flex-grow-1 messages-section">
                <div class="chat-status text-right">&nbsp;</div>
                <div class="chat-messages"></div>
                <div class=" textarea">
                    <?php if(!empty($to)) { ?>
                        <textarea name="chat-input" isAdmin=true placeholder="type here..." autofocus></textarea>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/e94e85d680.js" crossorigin="anonymous"></script>
<script src="/canarychat/assets/admin.js?v=<?php echo time() ?>"></script>

</body>
</html>
