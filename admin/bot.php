<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$q = filter_input(INPUT_POST, "q", FILTER_SANITIZE_STRING);
$q = trim($q);

// log
if ($_POST["logChat"] == "true") {
    $userId = filter_input(INPUT_POST, "userId", FILTER_SANITIZE_STRING);
    $file = "connections.json";
    $connections = file_exists($file)? json_decode(file_get_contents($file)) : [];
    $message = array("id"=>null, "type"=>"message", "admin"=>false, "body"=>$q);
    $newConn = array(
        "id"=>null,
        "admin"=>false,
        "userId"=>$userId,
        "alive"=>true,
        "messages"=>[$message]
    );
    $i=0; $found = false;
    if (!empty($connections)) {
        foreach($connections as $conn) {
            if ($conn->userId == $userId) {
                array_push($conn->messages, $message);
                $connections[$i] = $conn;
                $found = true;
            }
            $i++;
        }
        if (!$found) {
            array_push($connections, $newConn);
        }
    } else {
        $connections = [$newConn];
    }
    file_put_contents($file, json_encode($connections, JSON_PRETTY_PRINT));
}


// return automated bot responses with format

// echo json_encode(array(
//     "found"=>true,
//     "body"=>$response
// ));
 