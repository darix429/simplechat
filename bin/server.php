<?php

// namespace ChatBot;

require "/vendor/autoload.php";

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\Http\Router;
use Ratchet\Http\HttpServerInterface;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;

use Psr\Http\Message\RequestInterface;


class Chat implements MessageComponentInterface {
    protected $clients;
    public $file;
    public $ADMIN;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->file = "localhost/canarychat/admin/connections.json";
        
        if(!file_exists($this->file))
        file_put_contents($this->file, "[]");

        $this->bootConnections();
    }
    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        $id = $conn->resourceId;
        // echo "New Connection :: $id\n";

        $query = $conn->httpRequest->getUri()->getQuery();
        parse_str($query, $query);
        $this->updateConnections($conn, $query);
    }
    
    public function getConnections() {
        return file_exists($this->file)? json_decode(file_get_contents($this->file)) : [];
    }

    public function bootConnections() {
        $connections = $this->getConnections();
        for($i=0; $i<count($connections); $i++) {
            $connections[$i]->alive = false;
        }
        file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));
    }

    public function updateConnections($newClient, $q = null) {
        $isAdmin = isset($q["a"])? true: false;
        $userId = isset($q["user"])? $q["user"]: null;
        $connections = $this->getConnections();

        $newConn = array(
            "id"=>$newClient->resourceId,
            "admin"=>$isAdmin,
            "userId"=>$userId,
            "alive"=>true,
            "messages"=>array()
        );

        if ($isAdmin) {
            // echo "ADMIN DETECTED :: " . $newClient->resourceId . "\n";
            $this->ADMIN = $newClient;
        }

        if (!empty($connections)) {
            $i=0; $found = false;
            foreach($connections as $conn) {
                if ($conn->userId == $userId) {
                    // resume connection
                    $conn->id = $newClient->resourceId;
                    $conn->alive = true;
                    $connections[$i]=$conn;
                    if (!empty($conn->messages)) {
                        $seed = array(
                            "id"=>$newClient->resourceId,
                            "type"=>"seed-message",
                            "admin"=>false,
                            "body"=>$conn->messages
                        );
                        $newClient->send(json_encode($seed));
                    }
                    // notify admin
                    if (!empty($this->ADMIN)){
                        $this->ADMIN->send(json_encode(array(
                            "id"=>$conn->id,
                            "type"=>"ui-user-status",
                            "admin"=>false,
                            "body"=>array("userId"=>$userId, "alive"=>true)
                        )));
                    }
                }
                if ($conn->id == $newClient->resourceId) {
                    // update if Admin
                    $conn->admin = $isAdmin;
                    $connections[$i]=$conn;
                    $found = true;
                }
                $i++;
            }

            if (!$found) {
                // new connection
                array_push($connections, $newConn);
                if (!$isAdmin && !empty($this->ADMIN)) {
                    $this->ADMIN->send(json_encode(array(
                        "id"=>$conn->id,
                        "type"=>"ui-new-connection",
                        "admin"=>false,
                        "body"=>array("userId"=>$userId, "alive"=>true)
                    )));
                }
            }
        } else {
            $connections = [$newConn];
        }
        file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));
        $this->broadcastAdminStatus();
    }

    public function broadcastAdminStatus() {
        $adminStatus = empty($this->ADMIN)? false: true;
        $ui = array(
            "id"=>null,
            "type"=>"ui-admin-status",
            "admin"=>false,
            "body"=>$adminStatus
        );
        foreach($this->clients as $client) {
            $client->send(json_encode($ui));
        }
    }

    public function onMessage(ConnectionInterface $sender, $data) {
        $connections = $this->getConnections();
        $data = json_decode($data);
        $data->id = $sender->resourceId;

        $connId = $data->id;
        if ($data->admin) {
            // send to user
            $userId = $data->to;
            foreach($connections as $conn) {
                if ($conn->userId == $userId) {
                    $connId = $conn->id;
                }
            }
            // echo "SEND TO USER\n";
            $data->to = $connId;
            $this->sendToUser($data);
        } else {
            foreach($connections as $conn) {
                if ($conn->id == $data->id) {
                    $data->to = $conn->userId;
                }
            }
            echo "SEND TO ADMIN\n";
            $this->sendToAdmin($data);
        }

        // log
        $i=0;
        foreach($connections as $conn) {
            if ($conn->id == $connId) {
                if (isset($conn->messages)) {
                    array_push($conn->messages, $data);
                } else {
                    $conn->messages = [$data];
                }
                $connections[$i] = $conn;
            }
            $i++;
        }
        file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));
    }

    public function sendToAdmin($data) {
        if (!empty($data) && !empty($this->ADMIN)) {
            $this->ADMIN->send(json_encode($data));
        }
    }
    public function sendToUser($data) {
        foreach($this->clients as $client) {
            if ($client->resourceId == $data->to) {
                $client->send(json_encode($data));
            break;
            }
        }
    }

    public function onClose(ConnectionInterface $client) {
        // echo "Connection Disconnected :: ".$client->resourceId." \n";
        $connections = $this->getConnections();
        $id = $client->resourceId;
        $i = 0; $userId = null;
        foreach($connections as $conn) {
            if ($conn->id == $id) {
                // unset($connections[$i]);
                $userId = $conn->userId;
                $connections[$i]->alive = false;
            }
            $i++;
        }
        $connections = array_values($connections);
        file_put_contents($this->file, json_encode($connections, JSON_PRETTY_PRINT));

        // if admin
        if (!empty($this->ADMIN)) {
            if ($id == $this->ADMIN->resourceId) {
                // echo "ADMIN DISCONNECTED\n";
                $this->ADMIN = null;
                $this->broadcastAdminStatus();
            } else {
                $this->ADMIN->send(json_encode(array(
                    "id"=>$id,
                    "type"=>"ui-user-status",
                    "admin"=>false,
                    "body"=>array("userId"=>$userId, "alive"=>false)
                )));
            }
        }

        // detach
        $this->clients->detach($client);
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
        // echo "Error: {$e->getMessage()}\n";
        $conn->close();
    }

}



// SERVER
$routes = new RouteCollection;
$loop = React\EventLoop\Factory::create();

$wsServer = new WsServer(new Chat);
$wsServer->enableKeepAlive($loop);

$server = IoServer::factory(
    new HttpServer(
        $wsServer
    )
    , 8443
);
$server->run();